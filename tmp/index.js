import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Upload from '../views/Upload.vue'
import Keycloak from 'keycloak-js'
import { EventBus } from '../event-bus.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/upload',
    name: 'Upload',
    //component: () => import(/* webpackChunkName: "about" */ '../views/Upload.vue'),
    component: Upload,
    meta: {
     requiresAuth: true
    }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/booking',
    name: 'Booking',
    component: () => import(/* webpackChunkName: "booking" */ '../views/Booking.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  //base: proces.env.BASE_URL,
  routes
})

// function sleep(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms))
// }

let initOptions = {
	url: 'https://eyecandygallerydemo.duckdns.org/auth', realm: 'GalleryApiAuth', clientId: 'vueapp', onLoad: 'login-required'
}
let keycloak = Keycloak(initOptions);
Vue.prototype.$keycloak = keycloak
//Object.definePrototype(Vue.prototype, '$keycloak', { value: Keycloak(initOptions) });

//Object.defineProperty(Vue.prototype, 'keycloak', { value: Keycloak(initOptions) });

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    keycloak.init({ onLoad: initOptions.onLoad }).then((auth) => {
      if (!auth) {
        window.location.reload();
      }
      
      EventBus.$emit('login');
      // let payload = {
      //   idToken: keycloak.idToken,
      //   accessToken: keycloak.token
      // }

      //localStorage.setItem("vue-token", keycloak.token);
      //localStorage.setItem("vue-refresh-token", keycloak.refreshToken);
      //localStorage.setItem("vue-id-token", keycloak.idToken);
      //localStorage.setItem("vue-isAuthenticated", keycloak.authenticated);

      // if (keycloak.token && keycloak.idToken && keycloak.token != '' && keycloak.idToken != '') {
      //   this.$store.commit('login', payload);
      //   console.log("User has logged in: " + keycloak.subject)
      //   EventBus.$emit('login');
      // }
      // else {
      //   this.$store.commit("logout");
      //   EventBus.$emit('logout');
      //   //localStorage.setItem("vue-isAuthenticated", keycloak.authenticated);
      // }

      //next({redirect: {name: "Upload"}});

      //token refresh
      setInterval(() => {
        keycloak.updateToken(70).then((refreshed) => {
          if (refreshed) {
            Vue.$log.info('Token refreshed' + refreshed);
          } else {
            Vue.$log.info('Token not refreshed');
            EventBus.$emit('logout');
          }
        }).catch(() => {
          //EventBus.$emit('logout');
          //Vue.$log.error('Failed to refresh token');
        });
      }, 6000)
    }).catch(() => {
      //Vue.$log.error("Authenticated Failed");
    })
    
    //localStorage.setItem("vue-isAuthenticated", keycloak.authenticated); //yikes?
    next();
    
    
    // while (!router.app.$keycloak.ready) {
    //  sleep(100)
    // }

    // if (router.app.$keycloak.authenticated) {
    //   next({
    //     redirect: {name: 'Upload'}
    //   });
      
    // } else {
    //   const loginUrl = router.app.$keycloak.createLoginUrl()
    //   window.location.replace(loginUrl)
    // }    
    // } else {
    //   next();
    // }
  } else {
    //localStorage.setItem("vue-isAuthenticated", keycloak.authenticated); //yikes
    next();
  }
})

export default router
