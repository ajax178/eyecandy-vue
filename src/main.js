import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from 'vuex'
//import VueKeyCloak from '@dsb-norge/vue-keycloak-js'

// Vue.config.productionTip = false
// Vue.config.devtools = true

// let initOptions = {
//  url: 'http://localhost:8080/auth', realm: 'GalleryApiAuth', clientId: 'vueapp', onLoad: 'login-required'
// }
 
// let keycloak = Keycloak(initOptions);
// //Object.defineProperty(Vue.prototype, 'keycloak', { value: Keycloak(initOptions) });

// keycloak.init({ onLoad: initOptions.onLoad }).then((auth) => {
//   if (!auth) {
//     window.location.reload();
//   } 
//   //Vue.$log.info("Authenticated");
//   new Vue({
//     store,
//     router,
//     render: h => h(App, { props: { keycloak: keycloak } })
//   }).$mount('#app')

//   let payload = {
//     idToken: keycloak.idToken,
//     accessToken: keycloak.token
//   }

//   localStorage.setItem("vue-token", keycloak.token);
//   localStorage.setItem("vue-refresh-token", keycloak.refreshToken);
//   localStorage.setItem("vue-id-token", keycloak.idToken);
  
//   if (keycloak.token && keycloak.idToken && keycloak.token != '' && keycloak.idToken != '') {
//     store.commit('login', payload);
//     console.log("User has logged in: " + keycloak.subject)
//   }
//   else {
//     store.commit("logout");
//   }

//   //Token Refresh
//   setInterval(() => {
//     keycloak.updateToken(70).then((refreshed) => {
//       if (refreshed) {
//         Vue.$log.info('Token refreshed' + refreshed);
//       } else {
//         //Vue.$log.warn('Token not refreshed, valid for '
//         //  + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
//       }
//     }).catch(() => {
//         //Vue.$log.error('Failed to refresh token');
//     });
//   }, 6000)

//   }).catch(() => {
//     //Vue.$log.error("Authenticated Failed");
// });





// Vue.use(VueKeyCloak, {
//   init: {
//       onLoad: 'login-required',
//       //silentCheckSsoRedirectUri: window.location.origin + "/silent-check-sso.html",
//       //onLoad: 'login-required',
//       //checkLoginIframe: false
//   },
//   config: {
//       realm: 'GalleryApiAuth',
//       url: 'http://localhost:8080/auth',
//       //redirectionUrl: 'http://127.0.0.1:8081',
//       clientId: 'vueapp'
//   },
//   //onReady: (keycloak) => {
//   //    console.log(`${keycloak}`)
//   //    new Vue({
//   //        el: '#app',
//   //        router,
//   //        template: '<App/>',
//   //        render: h => h(App)
//   //    }).$mount('#app')
//   //}
// })

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  render: h => h(App)
}).$mount('#app')



// new Vue({
//   router,
//   render: h => h(App)
// }).$mount('#app')